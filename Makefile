ifndef VERBOSE
.SILENT:
endif

PAS_FILES := $(wildcard *.pas)
EXE_FILES := $(PAS_FILES:.pas=)

.PHONY: all clean fpc

all: ${EXE_FILES}

clean:
	git clean -f -x -d

%: %.pas fpc
	fpc -v0 $<

fpc:
	bash -c 'type $@ >/dev/null 2>&1' || ( \
		echo "Please install Free Pascal."; \
		echo "See <https://wiki.freepascal.org/Installing_the_Free_Pascal_Compiler>"; \
		exit 1 \
	)
